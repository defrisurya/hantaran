<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/* Home Page */

Route::get('/', 'HomeController@index')->name('welcome');
Route::get('profil', 'HomeController@profile')->name('profil');
Route::get('about', 'HomeController@about')->name('about');
Route::get('contact', 'HomeController@contact')->name('contact');
Route::get('infoproduct/{id}', 'ProductController@show')->name('infoproduct');
Route::get('kategori/{id}', 'HomeController@kategori')->name('kategori');
/* End Home Page */

/* Login Page & Authentikasi */
Route::get('administrator', 'LoginController@index')->name('login');
Route::post('login', 'LoginController@login')->name('postlogin');
Route::get('logout', 'LoginController@logout')->name('logout');
/* End Login Page & Authentikasi */

/* Administrator */
Route::group(['middleware' => 'auth'], function () {
    Route::get('dashboard', 'HomeController@admin')->name('dashboard');

    /* Profile */
    Route::get('profile', 'ProfileController@index')->name('admin.profile');
    Route::get('profile/edit/{id}', 'ProfileController@edit')->name('profile.edit');
    Route::post('profile/update/{id}', 'ProfileController@update')->name('profile.update');

    /* Product */
    Route::get('product', 'ProductController@index')->name('admin.produk');
    Route::get('product/add', 'ProductController@create')->name('produk.add');
    Route::get('product/edit/{id}', 'ProductController@edit')->name('produk.edit');
    Route::post('product', 'ProductController@store')->name('produk.store');
    Route::post('product/update/{id}', 'ProductController@update')->name('produk.update');
    Route::get('product/{id}', 'ProductController@destroy')->name('produk.delete');

    /* Kategori */
    Route::get('kategori', 'KategoriController@index')->name('admin.kategori');
    Route::get('kategori/edit/{id}', 'KategoriController@edit')->name('kategori.edit');
    Route::post('kategori', 'KategoriController@store')->name('kategori.store');
    Route::post('kategori/update/{id}', 'KategoriController@update')->name('kategori.update');
    Route::get('kategori/delete/{id}', 'KategoriController@destroy')->name('kategori.delete');
});
/* End Administrator */
