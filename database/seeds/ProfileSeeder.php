<?php

use Illuminate\Database\Seeder;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'namaprofile' => 'Gallery Hantaran Seserahan dan Mahar',
            'email' => 'gallery@gmail.com',
            'no_tlp' => '085781234321',
            'alamat' => 'Banguntapan, Bantul',
            'deskripsi' => 'Gallery Mahar dan Seserahan',
            'foto1' => 'images (1).jpg',
            'foto2' => 'images (2).jpg',
            'foto3' => 'images (3).jpg'
        ];
        \DB::table('profiles')->insert($data);
    }
}
