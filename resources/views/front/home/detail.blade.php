@extends('front.layout.app')

@section('title', 'Info Produk')

@section('content')
    <!-- Navbar -->
    <nav class="navbar navbar-light bg-light my-2">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{ route('welcome') }}">
                <img src="{{ asset('../assets/img/favicon.ico') }}" alt="" width="35px" height="35px" class="d-inline-block align-text-top">
            </a>
            <ul class="nav nav-tabs d-flex">
                <li class="nav-item">
                    <a class="nav-link @if (Request::segment(1) == '') active @endif" href="{{ route('welcome') }}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @if (Request::segment(1) == 'profil') active @endif" href="{{ route('profil') }}">Profile</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @if (Request::segment(1) == 'contact') active @endif" href="{{ route('contact') }}">Contact Us</a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- End Navbar -->

    <div class="container-fluid">
        <div class="row with-3d-shadow">
            <div class="col-12 mt-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-5 col-md-6 mb-xl-0 mb-4">
                                <div class="card border-2 card-blog card-plain">
                                    <div class="position-relative">
                                        <div id="carouselExampleIndicators" class="carousel slide" data-interval="false">
                                            <!-- The slideshow -->
                                            <div class="carousel-inner">
                                                <div class="carousel-item active">
                                                    <img src="{{ asset('uploads/'.$produk->foto1) }}" class="d-block w-100 shadow border-radius-xl" alt="img-blur-shadow" height="300px">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="{{ asset('uploads/'.$produk->foto2) }}" class="d-block w-100 shadow border-radius-xl" alt="img-blur-shadow" height="300px">
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="{{ asset('uploads/'.$produk->foto3) }}" class="d-block w-100 shadow border-radius-xl" alt="img-blur-shadow" height="300px">
                                                </div>
                                            </div>
                                            <!-- Left and right controls -->
                                            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Previous</span>
                                            </button>
                                            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Next</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-7 col-md-6 mb-xl-0 mb-4">
                                <div class="position-relative">
                                    <p style="color: black"><b>Kategori Produk :</b> {{ $produk->kategori->kategori }}</p>
                                    <p style="color: black"><b>Nama Produk :</b> {{ $produk->nama }}</p>
                                    <p style="color: black"><b>Harga Produk :</b> Rp. {{ number_format($produk->harga) }}</p>
                                    <p style="color: black"><b>Deskripsi Produk :</b><br><br> {{ $produk->deskripsi }}</p><br>
                                    <p>
                                        <p style="color: black">Anda Berminat ? Segera Hubungi Kami</p>
                                    </p>
                                    <p>
                                        <a class="btn btn-success mt-2 fs-6" href="https://wa.me/no_tlp"><i class="fab fa-whatsapp"></i>&nbsp;&nbsp;Whatsapp</a>
                                    </p>
                                </div>
                            </div>
                            <div class="card-header pb-0 mt-3">
                                <h5 style="color: black">Produk Lainnya</h5>
                                <hr style="color: black">
                            </div>
                            <div class="card-body p-3">
                                <div class="row">
                                    @foreach ($data as $item)
                                        <div class="col-xl-3 col-md-3 mb-4 mt-3">
                                            <div class="position-relative">
                                                <a href="{{ route('infoproduct', $item->id) }}" class="d-block">
                                                    <img src="{{ asset('uploads/'.$item->foto1) }}" alt="img-blur-shadow" height="200px" class="d-block w-100 mb-2">
                                                    <p style="color: black"><b>{{ $item->nama }}</b></p>
                                                    <p style="color: red"><b>Rp. {{ number_format($item->harga) }}</b></p>
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('front.layout.component.footer')
@endsection
