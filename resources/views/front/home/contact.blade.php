@extends('front.layout.app')

@section('title', 'Contact Us')

@section('content')
    <!-- Navbar -->
    <nav class="navbar navbar-light bg-light my-2">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{ route('welcome') }}">
                <img src="{{ asset('../assets/img/favicon.ico') }}" alt="" width="35px" height="35px" class="d-inline-block align-text-top">
            </a>
            <ul class="nav nav-tabs d-flex">
                <li class="nav-item">
                    <a class="nav-link @if (Request::segment(1) == '') active @endif" href="{{ route('welcome') }}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @if (Request::segment(1) == 'profil') active @endif" href="{{ route('profil') }}">Profile</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @if (Request::segment(1) == 'contact') active @endif" href="{{ route('contact') }}">Contact Us</a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- End Navbar -->

    <!-- Content Profile-->
    <div class="container-fluid py-3">
        <div class="row with-3d-shadow">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header text-center pb-0 p-3 mb-3">
                        <h5 class="mb-0" style="color: black">Contact Us</h5>
                        <hr style="color: black">
                    </div>
                    <p class="text-center" style="color: black">
                        Nama : {{ $profile->namaprofile }} <br><br>

                        Email   : {{ $profile->email }} <br><br>

                        Alamat   : {{ $profile->alamat }} <br><br>

                        Phone : {{ $profile->no_tlp }}
                </div>
            </div>
        </div>
    </div>
    <!-- End Content Profile -->

    @include('front.layout.component.footer')
@endsection
