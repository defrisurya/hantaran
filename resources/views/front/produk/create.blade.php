@extends('front.layout.admin_layouts')

@section('title', 'Administrator | Produk')

@section('content')
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
        <div class="container-fluid py-1 px-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                    <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('dashboard') }}">Administrator</a></li>
                    <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('admin.produk') }}">Produk</a></li>
                    <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Tambah Produk</li>
                </ol>
                <h6 class="font-weight-bolder mb-0">Tambah Produk</h6>
            </nav>
            <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
                <div class="ms-md-auto pe-md-3 d-flex align-items-center">
                </div>
                <ul class="navbar-nav  justify-content-end">
                    <li class="nav-item d-flex align-items-center">
                        <i class="fa fa-user"></i>
                        <span class="d-sm-inline text-sm">&nbsp;{{ Auth::user()->name }}</span>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->

    <!-- Forms -->
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 p-3">
                        <div class="row">
                            <div class="col-6 d-flex align-items-center">
                            </div>
                        </div>
                    </div>
                    <div class="card-body pb-0 p-3">
                        <div class="container">
                            <form class="row g-3" method="POST" action="{{ route('produk.store') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="col-md-5">
                                    <label for="nama" class="form-label">Nama Produk</label>
                                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Produk">
                                </div>
                                <div class="col-md-3">
                                    <label for="kategori" class="form-label">Kategori Produk</label>
                                    <select class="form-control select2" name="kategori_id" id="kategori_id">
                                        <option value="">-- Pilih Kategori --</option>
                                        @foreach($kategori as $cat)
                                            <option value="{{$cat->id}}">{{$cat->kategori}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="harga" class="form-label">Harga Produk</label>
                                    <input type="number" class="form-control" id="harga" name="harga" placeholder="Harga Produk">
                                </div>
                                <div class="col-12">
                                    <label for="formFile" class="form-label">Upload Foto Pertama</label>
                                    <input class="form-control" type="file" id="foto1" name="foto1">
                                </div>
                                <div class="col-12">
                                    <label for="formFile" class="form-label">Upload Foto Kedua</label>
                                    <input class="form-control" type="file" id="foto2" name="foto2">
                                </div>
                                <div class="col-12">
                                    <label for="formFile" class="form-label">Upload Foto Ketiga</label>
                                    <input class="form-control" type="file" id="foto3" name="foto3">
                                </div>
                                <div class="col-md-12">
                                    <label for="deskripsi" class="form-label">Deskripsi Produk</label>
                                    <input type="text" class="form-control" id="deskripsi" name="deskripsi" placeholder="Deskripsi Produk">
                                </div>
                                <div class="col-12">
                                    <a href="{{ route('admin.produk') }}" type="button" class="btn btn-secondary float-start">Batal</a>
                                    <button type="submit" class="btn btn-primary float-end">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-10">
            @include('front.layout.component.footer')
        </div>
    </div>
@endsection
@section('script')
<script>
    $('.select2').select2({
    });

    CKEDITOR.replace( 'deskripsi' );
</script>
@endsection
