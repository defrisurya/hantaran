@extends('front.layout.admin_layouts')

@section('title', 'Administrator | Profile')

@section('content')
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
        <div class="container-fluid py-1 px-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                    <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('dashboard') }}">Administrator</a></li>
                    <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('admin.profile') }}">Profile Gallery</a></li>
                    <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Edit Profile Gallery</li>
                </ol>
                <h6 class="font-weight-bolder mb-0">Edit Profile Gallery</h6>
            </nav>
            <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
                <div class="ms-md-auto pe-md-3 d-flex align-items-center">
                </div>
                <ul class="navbar-nav  justify-content-end">
                    <li class="nav-item d-flex align-items-center">
                        <i class="fa fa-user"></i>
                        <span class="d-sm-inline text-sm">&nbsp;{{ Auth::user()->name }}</span>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->

    <!-- Forms -->
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 p-3">
                        <div class="row">
                            <div class="col-6 d-flex align-items-center">
                            </div>
                        </div>
                    </div>
                    <div class="card-body pb-0 p-3">
                        <div class="container">
                            <form class="row g-3" method="POST" action="{{ route('profile.update', $profile->id) }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="col-md-5">
                                    <label for="namaprofile" class="form-label">Nama Perusahaan</label>
                                    <input type="text" class="form-control" id="namaprofile" name="namaprofile" placeholder="E-mail Gallery" value="{{ $profile->namaprofile }}">
                                </div>
                                <div class="col-md-4">
                                    <label for="email" class="form-label">E-mail</label>
                                    <input type="text" class="form-control" id="email" name="email" placeholder="E-mail Gallery" value="{{ $profile->email }}">
                                </div>
                                <div class="col-md-3">
                                    <label for="no_tlp" class="form-label">Nomor Telpon</label>
                                    <input type="number" class="form-control" id="no_tlp" name="no_tlp" placeholder="Nomor Telpon Gallery" value="{{ $profile->no_tlp }}">
                                </div>
                                <div class="col-12">
                                    <label for="formFile" class="form-label">Upload Foto 1</label>
                                    <input class="form-control" type="file" id="foto1" name="foto1">
                                </div>
                                <div class="col-12">
                                    <img src="{{ asset('uploads/'.$profile->foto1) }}" class="avatar avatar-sm me-3" alt="user1">
                                </div>
                                <div class="col-12">
                                    <label for="formFile" class="form-label">Upload Foto 2</label>
                                    <input class="form-control" type="file" id="foto2" name="foto2">
                                </div>
                                <div class="col-12">
                                    <img src="{{ asset('uploads/'.$profile->foto2) }}" class="avatar avatar-sm me-3" alt="user1">
                                </div>
                                <div class="col-12">
                                    <label for="formFile" class="form-label">Upload Foto 3</label>
                                    <input class="form-control" type="file" id="foto3" name="foto3">
                                </div>
                                <div class="col-12">
                                    <img src="{{ asset('uploads/'.$profile->foto3) }}" class="avatar avatar-sm me-3" alt="user1">
                                </div>
                                <div class="col-md-12">
                                    <label for="alamat" class="form-label">Alamat</label>
                                    <textarea type="text" class="form-control" id="alamat" name="alamat" placeholder="Alamat Gallery">{{ $profile->alamat}}</textarea>
                                </div>
                                <div class="col-md-12">
                                    <label for="deskripsi" class="form-label">Deskripsi</label>
                                    <textarea type="text" class="form-control" id="deskripsi" name="deskripsi">{{ $profile->deskripsi}}</textarea>
                                </div>
                                <div class="col-12">
                                    <a href="{{ route('admin.profile') }}" type="button" class="btn btn-secondary float-start">Batal</a>
                                    <button type="submit" class="btn btn-primary float-end">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-10">
            @include('front.layout.component.footer')
        </div>
    </div>
@endsection
@section('script')
<script>
    CKEDITOR.replace( 'deskripsi' );
</script>
@endsection
