@extends('front.layout.admin_layouts')

@section('title', 'Administrator | Profile')

@section('content')
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
        <div class="container-fluid py-1 px-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
                    <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{ route('dashboard') }}">Administrator</a></li>
                    <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Profile Gallery</li>
                </ol>
                <h6 class="font-weight-bolder mb-0">Profile Gallery</h6>
                <li class="nav-item d-xl-none mt-3 d-flex align-items-center">
                    <a href="#" class="nav-link text-body p-0" id="iconNavbarSidenav">
                        <div class="sidenav-toggler-inner">
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                        </div>
                    </a>
                </li>
            </nav>
            <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
                <div class="ms-md-auto pe-md-3 mt-3 d-flex align-items-center">
                </div>
                <ul class="navbar-nav  justify-content-end">
                    <li class="nav-item d-flex align-items-center">
                        <i class="fa fa-user"></i>
                        <span class="d-sm-inline text-sm">&nbsp;{{ Auth::user()->name }}</span>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->

    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0 p-3">
                        <div class="row">
                            <div class="col-6 d-flex align-items-center">
                                <h6 class="mb-0">Data Profile Gallery</h6>
                            </div>
                        </div>
                    </div>
                    <div class="card-body pb-0 p-3">
                        <div class="table-responsive p-0">
                            <table class="table align-items-center mb-0">
                                <thead>
                                    <tr>
                                        <th class="text-uppercase text-secondary text-dark text-xxs font-weight-bolder opacity-7 ps-2">No</th>
                                        <th class="text-uppercase text-secondary text-dark text-xxs font-weight-bolder opacity-7 ps-2">Nama Perusahaan</th>
                                        <th class="text-uppercase text-secondary text-dark text-xxs font-weight-bolder opacity-7 ps-2">E-mail</th>
                                        <th class="text-uppercase text-secondary text-dark text-xxs font-weight-bolder opacity-7 ps-2">Alamat</th>
                                        <th class="text-uppercase text-secondary text-dark text-xxs font-weight-bolder opacity-7 ps-2">Foto</th>
                                        <th class="text-uppercase text-secondary text-dark text-xxs font-weight-bolder opacity-7 ps-2">Deskripsi</th>
                                        <th class="text-uppercase text-secondary text-dark text-xxs font-weight-bolder opacity-7 ps-2">Nomor Telpon</th>
                                        <th class="text-secondary opacity-7"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no = 1
                                    @endphp
                                    @forelse ($profil as $data)
                                    <tr>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0">{{ $no++ }}</p>
                                        </td>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0">{{ $data->namaprofile }}</p>
                                        </td>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0">{{ $data->email }}</p>
                                        </td>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0">{{ $data->alamat }}</p>
                                        </td>
                                        <td>
                                            <img src="{{ asset('uploads/'.$data->foto1) }}" class="avatar avatar-sm me-3" alt="user1">
                                            <img src="{{ asset('uploads/'.$data->foto2) }}" class="avatar avatar-sm me-3" alt="user1">
                                            <img src="{{ asset('uploads/'.$data->foto3) }}" class="avatar avatar-sm me-3" alt="user1">
                                        </td>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0"><b class="text-xs font-weight-bold mb-0">{!! $data->deskripsi !!}</b></p>
                                        </td>
                                        <td>
                                            <p class="text-xs font-weight-bold mb-0">{{ $data->no_tlp }}</p>
                                        </td>
                                        <td>
                                            <a href="{{ route('profile.edit', $data->id) }}" class="badge badge-sm bg-gradient-primary" data-toggle="tooltip" data-original-title="Edit user">
                                                <i class="fa fa-pencil me-sm-1"></i>
                                            </a>
                                        </td>
                                        @empty
                                        <tr>
                                            <td class="text-sm" colspan="15" align="center"><i>Data tidak tersedia</i></td>
                                        </tr>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-10">
            @include('front.layout.component.footer')
        </div>
    </div>
@endsection


