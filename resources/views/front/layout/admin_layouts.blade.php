<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="{{ asset('../assets/img/favicon.ico') }}">

        <title>@yield('title')</title>

        <!-- Fonts and icons -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />

        <!-- Nucleo Icons -->
        <link href="../css/nucleo-icons.css" rel="stylesheet" />
        <link href="../css/nucleo-svg.css" rel="stylesheet" />

        <!-- Font Awesome Icons -->
        <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
        <link href="{{ asset('../css/nucleo-svg.css') }}" rel="stylesheet" />

        <!-- CSS Files -->
        <link id="pagestyle" href="{{ asset('../css/soft-ui-dashboard.css?v=1.0.3') }}" rel="stylesheet" />

        <!-- Select2 -->
        <link rel="stylesheet" href="{{asset('../select2/plugins/select2/css/select2.min.css')}}">
        <link rel="stylesheet" href="{{asset('../select2/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">

        <!-- Ck Editor -->
        <script type="text/javascript" src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    </head>

    <body class="g-sidenav-show  bg-gray-100">
        @include('sweetalert::alert')

        @include('front.layout.component.sidebar')

        <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg">
            @yield('content')
        </main>
        <!--   Core JS Files   -->
        <script src="{{asset('../js/core/popper.min.js')}}"></script>
        <script src="{{asset('../js/core/bootstrap.min.js')}}"></script>
        <script src="{{asset('../js/plugins/perfect-scrollbar.min.js')}}"></script>
        <script src="{{asset('../js/plugins/smooth-scrollbar.min.js')}}"></script>
        <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="{{asset('../js/soft-ui-dashboard.min.js?v=1.0.3')}}"></script>
        <script src="{{ asset('../select2/vendors/jquery/dist/jquery.min.js') }}"></script>

        <!-- Select2 -->
        <script src="{{asset('../select2/plugins/select2/js/select2.full.min.js')}}"></script>

        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

        @yield('script')
    </body>
</html>
