<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="base-url" content="{{ url('') }}">

        <title>@yield('title')</title>

        <!-- Favicon -->
        <link rel="icon" type="image/x-icon" href="{{ asset('../assets/img/favicon.ico') }}" />
        <link href="{{ asset('../bootstrap-5.1.3-dist/css/bootstrap.min.css') }}" rel="stylesheet">

        <!-- Core theme CSS (includes Bootstrap) -->
        <link href="{{ asset('../css/styles.css') }}" rel="stylesheet" />
        <script src="{{ asset('../bootstrap-5.1.3-dist/js/bootstrap.bundle.min.js') }}"></script>

        <!-- Fonts and icons -->
        <link href="https://fonts.googleapis.com/css2?family=Lora&display=swap:400,500,600" rel="stylesheet">

        <!-- Nucleo Icons -->
        <link href="../css/nucleo-icons.css" rel="stylesheet" />
        <link href="../css/nucleo-svg.css" rel="stylesheet" />

        <!-- Font Awesome Icons -->
        <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
        <link href="{{ asset('../css/nucleo-svg.css') }}" rel="stylesheet" />

        <!-- CSS Files -->
        <link id="pagestyle" href="{{ asset('../css/soft-ui-dashboard.css?v=1.0.3') }}" rel="stylesheet" />
        <link id="pagestyle" href="{{ asset('../css/front.css') }}" rel="stylesheet" />

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />

        <!-- Owl Stylesheets -->
        <link rel="stylesheet" href="../owlcarousel/assets/owl.carousel.min.css">
        <link rel="stylesheet" href="../owlcarousel/assets/owl.theme.default.min.css">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <!-- Favicons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="shortcut icon" href="../assets/ico/favicon.png">
        <link rel="shortcut icon" href="favicon.ico">

        <!-- Yeah i know js should not be in header. Its required for demos.-->

        <!-- javascript -->
        <script src="../vendors/jquery.min.js"></script>
        <script src="../owlcarousel/owl.carousel.js"></script>
    </head>
    <body>

        @yield('content')

        <!-- Core theme JS-->
        <script src="{{asset('../js/scripts.js')}}"></script>
    </body>
</html>
