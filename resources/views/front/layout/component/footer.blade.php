<section class="footer p-4 mt-4 text-sm bg-gradient-faded-dark text-white">
    <div class="row justify-content-center">
        <div class="col-md-4 text-center">
            <u><b>LEARN MORE</b></u> <br><br>
            <a href="{{ route('profil') }}" style="color: white">
                Profile
            </a><br>
            <a href="{{ route('contact') }}" style="color: white">
                Contact Us
            </a><br><br>
        </div>
        <div class="col-md-4 text-center">
            <u><b>CONTACT US</b></u> <br><br>
            Alamat   : {{ $profile->alamat }} <br>
            Email   : {{ $profile->email }} <br>
            Phone : {{ $profile->no_tlp }} <br>
        </div>
        <div class="col-md-4 text-center">
            <br><br>
            &copy;&nbsp;<script>
                document.write(new Date().getFullYear())
            </script>&nbsp;PT Inovasi Nuswantara Digital
        </div>
    </div>
</section>
