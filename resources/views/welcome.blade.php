@extends('front.layout.app')

@section('title', 'Galeri Seserahan')

@section('content')
    <!-- Navbar -->
    <nav class="navbar navbar-light bg-light my-2">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{ route('welcome') }}">
                <img src="{{ asset('../assets/img/favicon.ico') }}" alt="" width="35px" height="35px" class="d-inline-block align-text-top">
            </a>
            <ul class="nav nav-tabs d-flex">
                <li class="nav-item">
                    <a class="nav-link @if (Request::segment(1) == '') active @endif" href="{{ route('welcome') }}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @if (Request::segment(1) == 'profil') active @endif" href="{{ route('profil') }}">Profile</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @if (Request::segment(1) == 'about') active @endif" href="#kategori">Produk</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @if (Request::segment(1) == 'contact') active @endif" href="{{ route('contact') }}">Contact Us</a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- End Navbar -->

    <!-- Header-->
    <header class="bg-white bg-light">
        <div class="col-md-12">
            <div id="Headers" class="carousel slide" data-bs-ride="carousel">
                <!-- The slideshow -->
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="position-relative">
                            <img src="{{ asset('uploads/'.$profile->foto1) }}" class="d-block w-100" alt="img-blur-shadow" height="500px">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="position-relative">
                            <img src="{{ asset('uploads/'.$profile->foto2) }}" class="d-block w-100" alt="img-blur-shadow" height="500px">
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="position-relative">
                            <img src="{{ asset('uploads/'.$profile->foto3) }}" class="d-block w-100" alt="img-blur-shadow" height="500px">
                        </div>
                    </div>
                </div>
                <!-- Left and right controls -->
                <button class="carousel-control-prev" type="button" data-bs-target="#Headers" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#Headers" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>
        </div>
    </header>

    <!-- Kategori-->
    <div class="container-fluid" id="kategori">
        <div class="row with-3d-shadow">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-3" style="color: black">Kategori Produk</h5>
                        <hr style="color: black">
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="owl-carousel">
                                @foreach ($cat as $item)
                                    <div class="col-md-10">
                                        <div class="position-relative">
                                            <div class="card card-body border border-1 border-dark text-center">
                                                <a href="{{ url('kategori', $item->id) }}" class="d-block">
                                                    <img src="{{ asset('uploads/'.$item->foto) }}" class="w-100 d-inline" height="95px">
                                                    <p class="mt-2" style="color: black">{{ $item->kategori }}</p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                            <script>
                                $(document).ready(function() {
                                    var owl = $('.owl-carousel');
                                    owl.owlCarousel({
                                    margin: 10,
                                    nav: true,
                                    loop: true,
                                    responsive: {
                                        0: {
                                        items: 1
                                        },
                                        600: {
                                        items: 3
                                        },
                                        1000: {
                                        items: 5
                                        }
                                    }
                                    })
                                })
                            </script>
                        </div>
                    </div>
                    <div class="card-header pb-0 p-3 mt-3">
                        <h5 class="mb-0" style="color: black">Produk Terbaru</h5>
                        <hr style="color: black">
                    </div>
                    <div class="card-body p-3">
                        <div class="row">
                            @forelse ($product as $item)
                                <div class="col-xl-3 col-md-3 mb-4 mt-3">
                                    <div class="position-relative">
                                        <a href="{{ route('infoproduct', $item->id) }}" class="d-block">
                                            <img src="{{ asset('uploads/'.$item->foto1) }}" alt="img-blur-shadow" height="200px" class="d-block w-100 mb-2">
                                            <p style="color: black"><b>{{ $item->nama }}</b></p>
                                            <p style="color: red"><b>Rp. {{ number_format($item->harga) }}</b></p>
                                        </a>
                                    </div>
                                </div>
                                @empty
                                <p class="text-center" style="color: black"><i>Data tidak tersedia</i></p>
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('front.layout.component.footer')
@endsection
