<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [
        'kategori_id',
        'nama',
        'foto1',
        'foto2',
        'foto3',
        'deskripsi',
        'harga'
    ];

    public function kategori()
    {
        return $this->belongsTo(Kategori::class);
    }
}
