<?php

namespace App\Http\Controllers;

use App\Kategori;
use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Kategori::paginate(5);
        $profile = Profile::first();

        if ($request->cari) {
            if ($request->cari == "kategori") {
                $data = Kategori::where('kategori')->latest()->paginate(5);
            } else {
                $data = Kategori::where(function ($data) use ($request) {
                    $data->where('kategori', 'like', "%$request->cari%");
                })->latest()->paginate(5);
            }
        }

        return view('front.kategori.index', compact('data', 'profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nm = $request->foto;
        $namaFile = time() . rand(100, 999) . "." . $nm->getClientOriginalExtension();

        $cat = new Kategori;
        $cat->kategori = $request->kategori;
        $cat->foto = $namaFile;

        $nm->move(public_path() . '/uploads', $namaFile);
        $cat->save();

        return redirect()->back()->with('toast_success', 'Kategori Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function show(Kategori $kategori)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = Kategori::findorfail($id);
        $profile = Profile::first();

        return view('front.kategori.edit', compact('profile', 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ubah = Kategori::findorfail($id);
        $foto = $ubah->foto;

        $data = [
            'kategori' => $request['kategori'],
            'foto' => $foto
        ];

        if ($request->foto == null) {
            $ubah->update($data);
        } else {
            $request->foto->move(public_path() . '/uploads', $foto);
            $ubah->update($data);
        }

        return redirect('kategori')->with('toast_success', 'Kategori Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('kategoris')->where('id', $id)->delete();
        return redirect()->back()->with('toast_success', 'Kategori Berhasil Dihapus');
    }
}
