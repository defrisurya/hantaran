<?php

namespace App\Http\Controllers;

use App\Kategori;
use App\Product;
use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $produk = Product::paginate(5);
        $profile = Profile::first();

        if ($request->cari) {
            if ($request->cari == "nama") {
                $produk = Product::where('nama')->latest()->paginate(5);
            } else {
                $produk = Product::where(function ($produk) use ($request) {
                    $produk->where('nama', 'like', "%$request->cari%");
                    $produk->orWhere('harga', 'like', "%$request->cari%");
                })->latest()->paginate(5);
            }
        }

        return view('front.produk.index', compact('produk', 'profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $profile = Profile::first();
        $kategori = Kategori::all();
        return view('front.produk.create', compact('profile', 'kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nm1 = $request->foto1;
        $nm2 = $request->foto2;
        $nm3 = $request->foto3;
        $namaFile1 = time() . rand(100, 999) . "." . $nm1->getClientOriginalExtension();
        $namaFile2 = time() . rand(100, 999) . "." . $nm2->getClientOriginalExtension();
        $namaFile3 = time() . rand(100, 999) . "." . $nm3->getClientOriginalExtension();

        $produk = new Product;
        $produk->kategori_id = $request->kategori_id;
        $produk->nama = $request->nama;
        $produk->foto1 = $namaFile1;
        $produk->foto2 = $namaFile2;
        $produk->foto3 = $namaFile3;
        $produk->deskripsi = $request->deskripsi;
        $produk->harga = $request->harga;

        $nm1->move(public_path() . '/uploads', $namaFile1);
        $nm2->move(public_path() . '/uploads', $namaFile2);
        $nm3->move(public_path() . '/uploads', $namaFile3);
        // dd($produk);
        $produk->save();

        return redirect('product')->with('toast_success', 'Produk Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = Profile::first();
        $produk = Product::find($id);
        $data = Product::all();
        return view('front.home.detail', compact('produk', 'profile', 'data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produk = Product::findorfail($id);
        $kategori = Kategori::all();
        $profile = Profile::first();
        return view('front.produk.edit', compact('profile', 'produk', 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ubahprod = Product::findorfail($id);
        $first = $ubahprod->foto1;
        $second = $ubahprod->foto2;
        $third = $ubahprod->foto3;

        $dt = [
            'nama' => $request['nama'],
            'kategori_id' => $request['kategori_id'],
            'foto1' => $first,
            'foto2' => $second,
            'foto3' => $third,
            'deskripsi' => $request['deskripsi'],
            'harga' => $request['harga'],
        ];

        if ($request->foto1 == null) {
            $ubahprod->update($dt);
        } elseif ($request->foto2 == null) {
            $ubahprod->update($dt);
        } elseif ($request->foto3 == null) {
            $ubahprod->update($dt);
        } else {
            $request->foto1->move(public_path() . '/uploads', $first);
            $request->foto2->move(public_path() . '/uploads', $second);
            $request->foto3->move(public_path() . '/uploads', $third);
            $ubahprod->update($dt);
        }

        return redirect('product')->with('toast_success', 'Produk Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('products')->where('id', $id)->delete();
        return redirect()->back()->with('toast_success', 'Produk Berhasil Dihapus');
    }
}
