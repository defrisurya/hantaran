<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profil = Profile::all();
        $profile = Profile::first();
        return view('front.profile.index', compact('profil', 'profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = Profile::find($id)->first();
        return view('front.profile.edit', compact('profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ubah = Profile::findorfail($id);
        $profil1 = $ubah->foto1;
        $profil2 = $ubah->foto2;
        $profil3 = $ubah->foto3;

        $dt = [
            'namaprofile' => $request['namaprofile'],
            'email' => $request['email'],
            'foto1' => $profil1,
            'foto2' => $profil2,
            'foto3' => $profil3,
            'deskripsi' => $request['deskripsi'],
            'alamat' => $request['alamat'],
            'no_tlp' => $request['no_tlp']
        ];

        if ($request->foto1 == null) {
            $ubah->update($dt);
        } elseif ($request->foto2 == null) {
            $ubah->update($dt);
        } elseif ($request->foto3 == null) {
            $ubah->update($dt);
        } else {
            $request->foto1->move(public_path() . '/uploads', $profil1);
            $request->foto2->move(public_path() . '/uploads', $profil2);
            $request->foto3->move(public_path() . '/uploads', $profil3);
            $ubah->update($dt);
        }

        return redirect('profile')->with('toast_success', 'Profile Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
